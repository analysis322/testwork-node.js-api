var express = require('express');
var app = express();
var request = require('request');
//https://www.cbr-xml-daily.ru/daily_json.js
var data;
request('https://www.cbr-xml-daily.ru/daily_json.js', function (error, response, body) {
    if (!error && response.statusCode == 200) {
     data = JSON.parse(body);
    }
  });

var bodyParser = require('body-parser')
app.use( bodyParser.json() );
app.use(bodyParser.urlencoded({
  extended: true
}));

app.get('/', function (req, res) {
  res.send('Hello API');
});

app.listen(3012, function() {
  console.log('API app started');
});

app.post('/api/convert', function(req, res) {
var from = req.body.from.toUpperCase(),
	to = req.body.to.toUpperCase(),
	value = req.body.value;
if(from === 'RUB')
  var conv_value = value/data.Valute[to].Value*data.Valute[to].Nominal;
  else{
    if(to === 'RUB')
      var conv_value = value*data.Valute[from].Value/data.Valute[from].Nominal;
    else{
      var course_to = data.Valute[to].Value/data.Valute[to].Nominal;
      var course_from = data.Valute[from].Value/data.Valute[from].Nominal;
      var conv_value = value*course_from/course_to;
    }
  }
  res.json({status: 'susseful', value: conv_value});
});

app.get('/api/get-currency', function(req, res) {
  console.log(typeof data.Valute);
});
